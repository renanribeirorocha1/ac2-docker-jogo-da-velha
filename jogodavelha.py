def print_board(board):
    print('   |   |')
    print(' ' + board[0] + ' | ' + board[1] + ' | ' + board[2])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + board[3] + ' | ' + board[4] + ' | ' + board[5])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + board[6] + ' | ' + board[7] + ' | ' + board[8])
    print('   |   |')

def check_win(board, player):
    return ((board[0] == player and board[1] == player and board[2] == player) or
            (board[3] == player and board[4] == player and board[5] == player) or
            (board[6] == player and board[7] == player and board[8] == player) or
            (board[0] == player and board[3] == player and board[6] == player) or
            (board[1] == player and board[4] == player and board[7] == player) or
            (board[2] == player and board[5] == player and board[8] == player) or
            (board[0] == player and board[4] == player and board[8] == player) or
            (board[2] == player and board[4] == player and board[6] == player))

def check_tie(board):
    for i in board:
        if i == ' ':
            return False
    return True

def get_move(board, player):
    while True:
        move = input("Digite a posição (0-8) para jogar, jogador " + player + ": ")
        if not move.isdigit():
            print("Entrada inválida. Digite um número de 0 a 8.")
        else:
            move = int(move)
            if move < 0 or move > 8:
                print("Entrada inválida. Digite um número de 0 a 8.")
            elif board[move] != ' ':
                print("Essa posição já foi jogada. Escolha outra.")
            else:
                return move

def play_game():
    board = [' '] * 9
    players = ['X', 'O']
    current_player = players[0]
    while True:
        print_board(board)
        move = get_move(board, current_player)
        board[move] = current_player
        if check_win(board, current_player):
            print_board(board)
            print("Parabéns, jogador " + current_player + "! Você venceu!")
            break
        elif check_tie(board):
            print_board(board)
            print("Empate!")
            break
        else:
            current_player = players[(players.index(current_player) + 1) % 2]

play_game()
