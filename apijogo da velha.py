from flask import Flask, jsonify, request

app = Flask(__name__)

board = [
    [None, None, None],
    [None, None, None],
    [None, None, None]
]

current_player = 'X'

@app.route('/')
def index():
    return 'Tic Tac Toe API'

@app.route('/board')
def get_board():
    return jsonify(board)

@app.route('/move', methods=['POST'])
def make_move():
    global board, current_player

    x = request.json['x']
    y = request.json['y']

    if board[x][y] is not None:
        return jsonify({'error': 'Invalid move'})

    board[x][y] = current_player

    winner = check_winner()
    if winner is not None:
        return jsonify({'winner': winner})

    if check_tie():
        return jsonify({'winner': 'Tie'})

    current_player = 'O' if current_player == 'X' else 'X'

    return jsonify({'success': True})

def check_winner():
    for i in range(3):
        if board[i][0] == board[i][1] == board[i][2] and board[i][0] is not None:
            return board[i][0]

        if board[0][i] == board[1][i] == board[2][i] and board[0][i] is not None:
            return board[0][i]

    if board[0][0] == board[1][1] == board[2][2] and board[0][0] is not None:
        return board[0][0]

    if board[0][2] == board[1][1] == board[2][0] and board[0][2] is not None:
        return board[0][2]

    return None

def check_tie():
    for i in range(3):
        for j in range(3):
            if board[i][j] is None:
                return False
    return True

if __name__ == '__main__':
    app.run(debug=True)
